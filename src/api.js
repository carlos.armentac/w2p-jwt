import axios from 'axios';

/**
 * Web2pyJWT
 * @constructor
 * @param {string} urlEndpoint - The URL of your Web2py API. Ex. http://127.0.1.1/welcome
 * @param {object} credentials - Login Credentials. Ex. { username: 'jonh@gmail.com', password: 'qwerty' }
 */
export default class Web2pyJWT {
  constructor(urlEndpoint, credentials) {
    let self = this
    this.credentials = credentials
    this.api = axios.create({ baseURL: urlEndpoint, Authorization: 'Bearer ' + self.token })
    this.token = ''
    this.logged = false
  }
  setupCredentials(credentials) { this.credentials = credentials }
  login() {
    let self = this
    return new Promise((resolve, reject) => {
      self.api.post('/default/user/jwt', self.credentials)
        .then(function (res) {
          if ('token' in res.data) {
            console.debug('Connected!. Token=', res.data.token);
            self.token = res.data.token;
            self.logged = true;
            resolve('JWT Auth success!');
          }
        })
        .catch(function (error) {
          self.logged = false;
          console.error(error);
          reject('ERROR: Bad credentials.');
        })
    })
  }
  renewToken() { return this.login() }
}
