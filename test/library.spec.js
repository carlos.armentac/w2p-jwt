/* global describe, it, before */
import chai from 'chai';
import { Web2pyJWT } from '../lib/index.js';

chai.expect();
const expect = chai.expect;
let lib;
const credentials = {username: 'carlos.armentac@gmail.com', password: 'carmenta'};

describe('Test Web2pyJWT instance with no logged in', () => {
  before(() => {
    lib = new Web2pyJWT('http://10.21.15.193/api');
    lib.setupCredentials(credentials);
    // lib.login();
  });
  describe('when created the instance Login', () => {
    it('should return the loggin status and credentials', () => {
      expect(lib.logged).to.be.equal(false);
      expect(lib.credentials).to.be.equal(credentials);
    });
  });
});

describe('Login into Web2pyJWT instance a valid log in', () => {
  before(() => {
    lib = new Web2pyJWT('http://10.21.15.193/api', credentials);
    lib.login();
  });
  describe('when I Login', () => {
    it('should return the loggin status and credentials', () => {
      expect(lib.logged).to.be.equal(true);
    });
  });
});