# w2p-jwt

Integrate easily JWT to your Javascript App using Web2Py with this Webpack based Library.

## Features

* Webpack 3 based.
* ES6 as a source.
* Easily get logged to a Web2Py APP

## Getting started

1. Build your library
  * Run `npm install` to get the project's dependencies
  * Run `npm run build` to produce minified version of your library.

## Scripts

* `npm run build` - produces production version of your library under the `lib` folder
* `npm run dev` - produces development version of your library and runs a watcher
* `npm run test` - well ... it runs the tests :)
* `npm run test:watch` - same as above but in a watch mode

## Usage

Class constructor:
/*
 * Web2pyJWT
 * @constructor
 * @param {string} urlEndpoint - The URL of your Web2py API. Ex. http://127.0.1.1/welcome
 * @param {object} credentials - Login Credentials. Ex. { username: 'jonh@gmail.com', password: 'qwerty' }
 */

 * Please check examples/index.html

 ## NOTE: This is work In Progress!!!!! Not for Production Ready.